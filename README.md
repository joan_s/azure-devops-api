# PHP Azure DevOps API
A simple PHP wrapper for Azure DevOps API.

## Install 
Use [Composer](https://getcomposer.org/) to install the library.

```sh
php composer.phar require joan_s/azure-devops-api
```

### Running unit tests
You can run unit test to make sure the library will work properly. 

```sh
vendor/bin/phpunit vendor/joan_s/azure-devops-api
```

## Usage
### Authorize access with OAuth 2.0
Before getting started you will need to register you app and get an app ID from Azure DevOps. 

Then your users will need to authorize your app on their accounts. You will get an authorization code from that process, used to obtain an access token which will allow you to send request to the API.

You can follow the [Azure DevOps OAuth documentation](https://docs.microsoft.com/en-us/azure/devops/integrate/get-started/authentication/oauth?view=azure-devops) on how to obtain an authorization code.

### Getting an access token
Once you have an authorization code for your app, we provide an OAuth Client to obtain the access token you will need to authenticate your requests to the API.

```php
...

$oAuthClient = new AzureDevOps\OAuthClient('app_client_secret', 'app_redirect_uri');

$response = $oAuthClient->getOAuthTokens('authorization_code');

$accessToken = $response['access_token'];
$refreshToken = $response['refresh_token'];

```

You should persist the refresh token to be able to get a new access token after it expire.

### Refresh access token
Refreshing the access token is the same as getting one. Use the refresh token instead of the authorization code.

```php
...

$response = $oAuthClient->getOAuthTokens('refresh_token');

$newAccessToken = $response['access_token'];
$newRefreshToken = $response['refresh_token'];

```

### Use access token
```php
...

// Instantiate a client.
$client = new AzureDevOps\Client('access_token', 'https://dev.azure.com/organization');

// Get the first project of your organization.
$projects = $client->core->projects->list(['$top' => 1]);

// Extract project id from the response.
$projectId = $projects['value'][0]['id'];

// Get all workItems for this project, expanding all their fields.
$workItems = $client->workItemTracking->workItems->list($projectId, ['$expand' => 'fields']);

```

For more information about available endpoints, see the [Azure DevOps API documentation](https://docs.microsoft.com/en-us/rest/api/azure/devops/).

## Thanks

* Thanks to [Kevin Saliou](https://github.com/kbsali) for the [php-redime-api](https://github.com/kbsali/php-redmine-api) library, which inspired this.
