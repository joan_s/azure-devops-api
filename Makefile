gc -am .PHONY: install

intall:
	composer install
	git config branch.autosetuprebase always

cs:
	vendor/bin/php-cs-fixer fix --config=.php_cs.dist --verbose --diff

test: 
	vendor/bin/phpunit --verbose --coverage-text