#!/bin/bash

# Docker only
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git zip unzip which are required by composer.
apt-get update -y
apt-get install git zip unzip -y

# Install XDebug to get php-code-coverage component for php-unit.
pecl install xdebug
docker-php-ext-enable xdebug

# Install Composer (https://getcomposer.org/download/).
curl https://composer.github.io/installer.sig -O | tr -d '\n' > installer.sig
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === file_get_contents('installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php'); unlink('installer.sig');"
