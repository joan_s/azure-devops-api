<?php

namespace AzureDevOps\Api\Work;

use AzureDevOps\Api\AbstractApi;

/**
 * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/work/iterations
 *
 * @method array list
 *
 * @author Joan Souliard <joan dot souliard at pm dot me>
 */
class Iterations extends AbstractApi
{
    private const ENDPOINT = '%s/_apis/work/teamsettings/iterations';

    /**
     * Get a team's iterations using timeframe filter.
     *
     * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/work/iterations/list
     *
     * @param string      $project                  project ID or project name
     * @param string|null $team                     team ID or team name
     * @param array       $parameters
     * @param string      $parameters['$timeframe'] A filter for which iterations are returned based on relative time. Only Current is supported currently.
     * @param string      $parameters['api-version] Version of the API to use. This should be set to '5.1' to use this version of the api.
     *
     * @return array
     */
    public function list(string $project, ?string $team = null, array $parameters = []): array
    {
        return $this->get(sprintf(self::ENDPOINT, urlencode($project.'/'.$team)).$this->buildQueryParameters($parameters));
    }
}
