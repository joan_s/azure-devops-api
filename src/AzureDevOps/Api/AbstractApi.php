<?php

namespace AzureDevOps\Api;

use AzureDevOps\Client;
use AzureDevOps\CreateExceptionTrait;

abstract class AbstractApi
{
    use CreateExceptionTrait;

    private const DEFAULT_API_VERSION = '5.1';

    /** @var Client */
    protected $client;

    /**
     * AbstractApi constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Shortcut to call client get() method.
     *
     * @param string $endpoint
     *
     * @return array
     */
    protected function get(string $endpoint): array
    {
        return $this->client->get($endpoint);
    }

    /**
     * Shortcut to call client post() method.
     *
     * @param string $endpoint
     * @param array $data
     *
     * @return array
     */
    protected function post(string $endpoint, array $data = [], ?string $contentType = null): array
    {
        return $this->client->post($endpoint, $data, $contentType);
    }

    /**
     * Shortcut to call client patch() method.
     *
     * @param string $endpoint
     * @param array  $data
     *
     * @return array
     */
    protected function patch(string $endpoint, array $data = [], ?string $contentType = null): array
    {
        return $this->client->patch($endpoint, $data, $contentType);
    }

    /**
     * Generate URL-encoded query string.
     *
     * @param array $parameters
     *
     * @return string
     */
    protected function buildQueryParameters(array $parameters = []): string
    {
        // Force default api version if not specified.
        if (!isset($parameters['api-version'])) {
            $parameters['api-version'] = self::DEFAULT_API_VERSION;
        }

        return '?'.http_build_query($parameters);
    }
}
