<?php

namespace AzureDevOps\Api;

/**
 * Azure DevOps WorkItemTracking service representation.
 *
 * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/wit/
 *
 * @method AbstractApi $attachments
 * @method AbstractApi $classificationNodes
 * @method AbstractApi $fields
 * @method AbstractApi $workItems
 * @method AbstractApi $workItemTypes
 * @method AbstractApi $workItemTypesField
 *
 * @author Joan Souliard <joan dot souliard at pm dot me>
 */
class WorkItemTracking extends AbstractService
{
    protected $classes = [
        'attachments' => 'Attachments',
        'classificationNodes' => 'ClassificationNodes',
        'fields' => 'Fields',
        'workItems' => 'WorkItems',
        'workItemTypes' => 'WorkItemTypes',
        'workItemTypesField' => 'WorkItemTypesField',
    ];
}
