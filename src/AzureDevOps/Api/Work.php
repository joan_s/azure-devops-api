<?php

namespace AzureDevOps\Api;

/**
 * Azure DevOps Work service representation.
 *
 * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/work
 *
 * @method AbstractApi $iterations
 *
 * @author Joan Souliard <joan dot souliard at pm dot me>
 */
class Work extends AbstractService
{
    protected $classes = [
        'iterations' => 'Iterations',
    ];
}
