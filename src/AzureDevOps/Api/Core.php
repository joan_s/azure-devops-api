<?php

namespace AzureDevOps\Api;

/**
 * Azure DevOps Core service representation.
 *
 * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/core/
 *
 * @method AbstractApi $projects
 * @method AbstractApi $teams
 *
 * @author Joan Souliard <joan dot souliard at pm dot me>
 */
class Core extends AbstractService
{
    protected $classes = [
        'projects' => 'Projects',
        'teams' => 'Teams',
    ];
}
