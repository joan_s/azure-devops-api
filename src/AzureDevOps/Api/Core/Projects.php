<?php

namespace AzureDevOps\Api\Core;

use AzureDevOps\Api\AbstractApi;

/**
 * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/core/projects
 *
 * @method array list
 * @method array getProject
 *
 * @author Joan Souliard <joan dot souliard at pm dot me>
 */
class Projects extends AbstractApi
{
    private const ENDPOINT = '/_apis/projects';

    /**
     * Get all projects in the organization that the authenticated user has access to.
     *
     * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/core/projects/list
     *
     * @param array  $parameters
     * @param string $parameters["stateFilter"]            Filter on team projects in a specific team project state (default: WellFormed)
     * @param int    $parameters["$top"]
     * @param int    $parameters["$skip"]
     * @param string $parameters["continuationToken"]
     * @param bool   $parameters["getDefaultTeamImageURL"]
     * @param string $parameters["api-version"]            Version of the API to use
     *
     * @return array
     */
    public function list(array $parameters = []): array
    {
        return $this->get(self::ENDPOINT.$this->buildQueryParameters($parameters));
    }

    /**
     * Get project with the specified id or name, optionally including capabilities.
     *
     * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/core/projects/get
     *
     * @param string $project                           Project ID or project name
     * @param array  $parameters
     * @param bool   $parameters["includeCapabilities"] Include capabilities (such as source control) in the team project result (default: false)
     * @param bool   $parameters["includeHistory"]      Search within renamed projects (that had such name in the past)
     * @param string $parameters["api-version"]         Version of the API to use
     *
     * @return array
     */
    public function getProject(string $project, array $parameters = []): array
    {
        return $this->get(self::ENDPOINT.'/'.urlencode($project).$this->buildQueryParameters($parameters));
    }
}
