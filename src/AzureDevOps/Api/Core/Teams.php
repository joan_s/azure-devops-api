<?php

namespace AzureDevOps\Api\Core;

use AzureDevOps\Api\AbstractApi;

/**
 * Manage teams in Azure DevOps Services. A DevOps account has one or more team projects.
 * Each team project has one or more teams that contribute to that project.
 *
 * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/core/teams
 *
 * @method array getProjectTeams
 * @method array getTeamMembers
 *
 * @author Joan Souliard <joan dot souliard at pm dot me>
 */
class Teams extends AbstractApi
{
    private const TEAMS_ENDPOINT = '/_apis/projects/%s/teams';
    private const MEMBERS_ENDPOINT = '/_apis/projects/%s/teams/%s/members';

    /**
     * Get a list of teams.
     *
     * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/core/teams/get
     *
     * @param string $project                       Project ID or project name
     * @param array  $parameters
     * @param bool   $parameters["$mine"]           If true return all the teams requesting user is member, otherwise return all the teams user has read access
     * @param int    $parameters["$top"]            Maximum number of teams to return
     * @param int    $parameters["$skip"]           Number of teams to skip
     * @param bool   $parameters["$expandIdentity"] A value indicating whether or not to expand Identity information in the result WebApiTeam object
     * @param string $parameters["api-version"]     Version of the API to use
     *
     * @return array
     */
    public function getProjectTeams(string $project, array $parameters = []): array
    {
        return $this->get(sprintf(self::TEAMS_ENDPOINT, urlencode($project)).$this->buildQueryParameters($parameters));
    }

    /**
     * Get a list of members for a specific team.
     *
     * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/core/teams/get%20team%20members%20with%20extended%20properties
     *
     * @param string $project                   Project ID or project name
     * @param string $team
     * @param array  $parameters
     * @param int    $parameters["$top"]        Maximum number of teams to return
     * @param int    $parameters["$skip"]       Number of teams to skip
     * @param string $parameters["api-version"] Version of the API to use
     *
     * @return array
     */
    public function getTeamMembers(string $project, string $team, array $parameters = []): array
    {
        return $this->get(sprintf(self::MEMBERS_ENDPOINT, urlencode($project), urlencode($team)).$this->buildQueryParameters($parameters));
    }
}
