<?php

namespace AzureDevOps\Api\WorkItemTracking;

use AzureDevOps\Api\AbstractApi;

/**
 * Project specific field definition API.
 *
 * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/wit/work%20item%20types%20field
 *
 * @method array list
 * @method array getWorkItemTypesField
 *
 * @author Joan Souliard <joan dot souliard at pm dot me>
 */
class WorkItemTypesField extends AbstractApi
{
    private const ENDPOINT = '%s/_apis/wit/workitemtypes/%s/fields';

    /**
     * Get a list of fields for a work item type with detailed references.
     *
     * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/wit/work%20item%20types%20field/list
     *
     * @param string $project                   Project ID or project name
     * @param string $type
     * @param array  $parameters
     * @param string $parameters["$expand"]     Expand level for the API response. Properties: to include allowedvalues, default value, isRequired etc. as a part of response; None: to skip these properties.
     * @param string $parameters["api-version"] Version of the API to use
     *
     * @return array
     */
    public function list(string $project, string $type, array $parameters = []): array
    {
        return $this->get(sprintf(self::ENDPOINT, urlencode($project), urlencode($type)).$this->buildQueryParameters($parameters));
    }

    /**
     * Get a field for a work item type with detailed references.
     *
     * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/wit/work%20item%20types%20field/get
     *
     * @param string $project                   Project ID or project name
     * @param string $type
     * @param string $field
     * @param string $parameters["$expand"]     Expand level for the API response. Properties: to include allowedvalues, default value, isRequired etc. as a part of response; None: to skip these properties.
     * @param string $parameters["api-version"] Version of the API to use
     * @param string
     *
     * @return array
     */
    public function getWorkItemTypesField(string $project, string $type, string $field, array $parameters = []): array
    {
        return $this->get(sprintf(self::ENDPOINT, urlencode($project), urlencode($type)).'/'.urlencode($field).$this->buildQueryParameters($parameters));
    }
}
