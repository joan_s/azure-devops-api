<?php

namespace AzureDevOps\Api\WorkItemTracking;

use AzureDevOps\Api\AbstractApi;
use AzureDevOps\Client;

/**
 * The Work Item API is used for listing, creating and updating work items.
 *
 * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/wit/work%20items
 *
 * @method array create
 * @method array update
 * @method array list
 * @method array getWorkItem
 * @method array getWorkItemTemplate
 *
 * @author Joan Souliard <joan dot souliard at pm dot me>
 */
class WorkItems extends AbstractApi
{
    private const ENDPOINT = '%s/_apis/wit/workitems';

    /**
     * Creates a single work item.
     *
     * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/wit/work%20items/create
     *
     * @param string $project                             project ID or project name
     * @param string $type                                the work item type of the work item to create
     * @param array  $parameters
     * @param array  $parameters["operations"]            Required. Operations to perform on the work items.
     * @param string $parameters["operations"][]["from"]  The path to copy from for the Move/Copy operation
     * @param string $parameters["operations"][]["op"]    The patch operation. Possible options are {add, copy, move, remove, replace, test}
     * @param string $parameters["operations"][]["path"]  The path for the operation. In the case of an array, a zero based index can be used to specify the position in the array (e.g. /biscuits/0/name). The "-" character can be used instead of an index to insert at the end of the array (e.g. /biscuits/-).
     * @param mixed  $parameters["operations"][]["value"] The value for the operation. This is either a primitive or a JToken.
     * @param bool   $parameters["validateOnly"]          Indicate if you only want to validate the changes without saving the work item
     * @param bool   $parameters["bypassRules"]           Do not enforce the work item type rules on this update
     * @param bool   $parameters["suppressNotification"]  Do not fire any notifications for this change
     * @param array  $parameters["$expand"]               The expand parameters for work item attributes. Possible options are { None, Relations, Fields, Links, All }.
     * @param string $parameters["api-version"]           Version of the API to use
     *
     * @return array
     */
    public function create(string $project, string $type, array $parameters = []): array
    {
        if (!isset($parameters['operations'])) {
            $this->createException('$parameters["operations"] index is required.');
        }

        $data = $parameters['operations'];
        $parameters['operations'] = null;

        $url = sprintf(self::ENDPOINT, urlencode($project)).'/$'.urlencode($type);

        return $this->post($url.$this->buildQueryParameters($parameters), $data, Client::APPLICATION_JSON_PATCH);
    }

    /**
     * Updates a single work item.
     *
     * @param string $project                             project ID or project name
     * @param int    $workItemId                          the id of the work item to update
     * @param array  $parameters
     * @param array  $parameters["operations"]            Required. Operations to perform on the work items.
     * @param string $parameters["operations"][]["from"]  The path to copy from for the Move/Copy operation
     * @param string $parameters["operations"][]["op"]    The patch operation. Possible options are {add, copy, move, remove, replace, test}
     * @param string $parameters["operations"][]["path"]  The path for the operation. In the case of an array, a zero based index can be used to specify the position in the array (e.g. /biscuits/0/name). The "-" character can be used instead of an index to insert at the end of the array (e.g. /biscuits/-).
     * @param mixed  $parameters["operations"][]["value"] The value for the operation. This is either a primitive or a JToken.
     * @param bool   $parameters["validateOnly"]          Indicate if you only want to validate the changes without saving the work item
     * @param bool   $parameters["bypassRules"]           Do not enforce the work item type rules on this update
     * @param bool   $parameters["suppressNotification"]  Do not fire any notifications for this change
     * @param array  $parameters["$expand"]               The expand parameters for work item attributes. Possible options are { None, Relations, Fields, Links, All }.
     * @param string $parameters["api-version"]           Version of the API to use
     *
     * @return array
     */
    public function update(string $project, int $workItemId, array $parameters = []): array
    {
        if (!isset($parameters['operations'])) {
            $this->createException('$parameters["operations"] index is required.');
        }

        $data = $parameters['operations'];
        $parameters['operations'] = null;

        $url = sprintf(self::ENDPOINT, urlencode($project)).'/'.urlencode($workItemId);

        return $this->patch($url.$this->buildQueryParameters($parameters), $data, Client::APPLICATION_JSON_PATCH);
    }

    /**
     * Returns a list of work items (Maximum 200).
     *
     * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/wit/work%20items/list
     *
     * @param string $project                   project ID or project name
     * @param array  $parameters
     * @param string $parameters["ids"]         Required - The comma-separated list of requested work item ids. (Maximum 200 ids allowed).
     * @param string $parameters["fields"]      Comma-separated list of requested fields
     * @param string $parameters["asOf"]        AsOf UTC date time string
     * @param string $parameters["$expand"]     The expand parameters for work item attributes. Possible options are { None, Relations, Fields, Links, All }.
     * @param string $parameters["errorPolicy"] The flag to control error policy in a bulk get work items request. Possible options are {Fail, Omit}.
     * @param string $parameters["api-version"] Version of the API to use
     *
     * @return array
     */
    public function list(string $project, array $parameters = []): array
    {
        return $this->get(sprintf(self::ENDPOINT, urlencode($project)).$this->buildQueryParameters($parameters));
    }

    /**
     * Returns a single work item.
     *
     * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/wit/work%20items/get%20work%20item
     *
     * @param string $project                   project ID or project name
     * @param string $workItem                  the work item id
     * @param array  $parameters
     * @param string $parameters["fields"]      Comma-separated list of requested fields
     * @param string $parameters["asOf"]        AsOf UTC date time string
     * @param string $parameters["$expand"]     The expand parameters for work item attributes. Possible options are { None, Relations, Fields, Links, All }.
     * @param string $parameters["api-version"] Version of the API to use
     *
     * @return array
     */
    public function getWorkItem(string $project, string $workItem, array $parameters = []): array
    {
        return $this->get(sprintf(self::ENDPOINT, urlencode($project)).'/'.urlencode($workItem).$this->buildQueryParameters($parameters));
    }

    /**
     * Returns a single work item from a template.
     *
     * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/wit/work%20items/get%20work%20item%20template
     *
     * @param string $project                   project ID or project name
     * @param string $type                      the work item type name
     * @param array  $parameters
     * @param string $parameters["fields"]      Comma-separated list of requested fields
     * @param string $parameters["asOf"]        AsOf UTC date time string
     * @param string $parameters["$expand"]     The expand parameters for work item attributes. Possible options are { None, Relations, Fields, Links, All }.
     * @param string $parameters["api-version"] Version of the API to use
     *
     * @return array
     */
    public function getWorkItemTemplate(string $project, string $type, array $parameters = []): array
    {
        return $this->get(sprintf(self::ENDPOINT, urlencode($project)).'/$'.urlencode($type).$this->buildQueryParameters($parameters));
    }
}
