<?php

namespace AzureDevOps\Api\WorkItemTracking;

use AzureDevOps\Api\AbstractApi;

/**
 * The controller that services the requests for Work Item Types.
 *
 * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/wit/work%20item%20types
 *
 * @method array list
 * @method array getWorkItemType
 *
 * @author Joan Souliard <joan dot souliard at pm dot me>
 */
class WorkItemTypes extends AbstractApi
{
    private const ENDPOINT = '%s/_apis/wit/workitemtypes';

    /**
     * Returns the list of work item types.
     *
     * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/wit/work%20item%20types/list
     *
     * @param string $project                   Project ID or project name
     * @param array  $parameters
     * @param string $parameters["api-version"] Version of the API to use
     *
     * @return array
     */
    public function list(string $project, array $parameters = []): array
    {
        return $this->get(sprintf(self::ENDPOINT, urlencode($project)).$this->buildQueryParameters($parameters));
    }

    /**
     * Returns a work item type definition.
     *
     * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/wit/work%20item%20types/get
     *
     * @param string $project                   Project ID or project name
     * @param string $type
     * @param array  $parameters
     * @param string $parameters["api-version"] Version of the API to use
     *
     * @return array
     */
    public function getWorkItemType(string $project, string $type, array $parameters = []): array
    {
        return $this->get(sprintf(self::ENDPOINT, urlencode($project)).'/'.urlencode($type).$this->buildQueryParameters($parameters));
    }
}
