<?php

namespace AzureDevOps\Api\WorkItemTracking;

use AzureDevOps\Api\AbstractApi;

/**
 * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/wit/classification%20nodes
 *
 * @method array getClassificationNodes
 *
 * @author Joan Souliard <joan dot souliard at pm dot me>
 */
class ClassificationNodes extends AbstractApi
{
    private const ENDPOINT = '%s/_apis/wit/classificationnodes';

    /**
     * Gets root classification nodes or list of classification nodes for a given list of nodes ids, for a given project.
     * In case ids parameter is supplied you will get list of classification nodes for those ids.
     * Otherwise you will get root classification nodes for this project.
     *
     * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/wit/classification%20nodes/get%20classification%20nodes
     *
     * @param string $project
     * @param array  $parameters
     * @param string $parameters['ids']         Comma separated integer classification nodes ids. It's not required, if you want root nodes.
     * @param int    $parameters['$depth']      Depth of children to fetch
     * @param string $parameters['errorPolicy'] Flag to handle errors in getting some nodes. Possible options are Fail and Omit.
     * @param string $parameters['api-version]  Version of the API to use. This should be set to '5.1' to use this version of the api.
     *
     * @return array
     */
    public function getClassificationNodes(string $project, array $parameters = []): array
    {
        return $this->get(sprintf(self::ENDPOINT, $project).$this->buildQueryParameters($parameters));
    }
}
