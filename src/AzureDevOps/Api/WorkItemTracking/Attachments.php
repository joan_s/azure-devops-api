<?php

namespace AzureDevOps\Api\WorkItemTracking;

use AzureDevOps\Api\AbstractApi;
use AzureDevOps\Client;

/**
 * Project specific Work Item attachments API.
 *
 * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/wit/attachments
 *
 * @method array create
 *
 * @author Joan Souliard <joan dot souliard at pm dot me>
 */
class Attachments extends AbstractApi
{
    private const ENDPOINT = '/_apis/wit/attachments';

    /**
     * Uploads an attachment.
     *
     * @param string $attachment                The attachment path
     * @param array  $parameters
     * @param string $parameters["fileName"]    The name of the file
     * @param string $parameters["uploadType"]  Attachment upload type: Simple or Chunked
     * @param string $parameters["areaPath"]    Target project Area Path
     * @param string $parameters["api-version"] Version of the API to use
     *
     * @return array
     */
    public function create(string $attachment, string $project = null, array $parameters = [])
    {
        $handle = fopen($attachment, 'r');
        $size = filesize($attachment);

        $file = [
            'file' => $handle,
            'size' => $size,
            'data' => fread($handle, $size),
        ];

        $url = empty($project) ? self::ENDPOINT : urlencode($project).self::ENDPOINT;

        return $this->post($url.$this->buildQueryParameters($parameters), $file, Client::APPLICATION_OCTET_STREAM);
    }
}
