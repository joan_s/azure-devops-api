<?php

namespace AzureDevOps\Api\WorkItemTracking;

use AzureDevOps\Api\AbstractApi;

/**
 * This controller version exists to support returning project scoped urls for fields.
 *
 * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/wit/fields
 *
 * @method array list
 * @method array getField
 *
 * @author Joan Souliard <joan dot souliard at pm dot me>
 */
class Fields extends AbstractApi
{
    private const ENDPOINT = '%s/_apis/wit/fields';

    /**
     * Returns information for all fields.
     *
     * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/wit/fields/list
     *
     * @param string $project                   Project ID or project name
     * @param array  $parameters
     * @param string $parameters["$expand"]     Use ExtensionFields to include extension fields, otherwise exclude them. Unless the feature flag for this parameter is enabled, extension fields are always included.
     * @param string $parameters["api-version"] Version of the API to use
     *
     * @return array
     */
    public function list(string $project, array $parameters = []): array
    {
        return $this->get(sprintf(self::ENDPOINT, urlencode($project)).$this->buildQueryParameters($parameters));
    }

    /**
     * Gets information on a specific field.
     *
     * @see https://docs.microsoft.com/en-us/rest/api/azure/devops/wit/fields/get
     *
     * @param string $project
     * @param string $field
     * @param array  $parameters
     * @param string $parameters["api-version"] Version of the API to use
     *
     * @return array
     */
    public function getField(string $project, string $field, array $parameters = []): array
    {
        return $this->get(sprintf(self::ENDPOINT, urlencode($project)).'/'.urlencode($field).$this->buildQueryParameters($parameters));
    }
}
