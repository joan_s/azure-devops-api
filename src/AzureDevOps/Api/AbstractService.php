<?php

namespace AzureDevOps\Api;

use AzureDevOps\Client;
use AzureDevOps\CreateExceptionTrait;

abstract class AbstractService
{
    use CreateExceptionTrait;

    /** @var array */
    protected $classes = [];

    /** @var array */
    protected $apis = [];

    /** @var Client */
    protected $client;

    /**
     * AbstractService Constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * PHP getter magic method.
     *
     * @param string $name
     *
     * @return Api\AbstractApi
     */
    public function __get(string $name)
    {
        return $this->api($name);
    }

    /**
     * Instantiate the api with the given name.
     *
     * @param string $name
     *
     * @throws \Exception
     *
     * @return AbstractApi
     */
    public function api(string $name): AbstractApi
    {
        if (!isset($this->classes[$name])) {
            throw $this->createException(sprintf('Invalid API name: %s.', $name));
        }

        if (isset($this->apis[$name])) {
            return $this->apis[$name];
        }

        $class = sprintf("%s\%s", get_class($this), $this->classes[$name]);
        $this->apis[$name] = new $class($this->client);

        return $this->apis[$name];
    }
}
