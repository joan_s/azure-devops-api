<?php

namespace AzureDevOps;

/**
 * CreateExceptionTrait.
 *
 * @method \Exception createException
 *
 * @author Joan Souliard <joan dot souliard at pm dot me>
 */
trait CreateExceptionTrait
{
    /**
     * Create an exception, with a message prefixed by the class, method and line where this method has been called.
     *
     * @param string $message
     * @param int    $code
     * @param bool   $warning
     *
     * @return \Exception
     */
    protected function createException(string $message = '', int $code = 0): \Exception
    {
        $trace = debug_backtrace();

        return new \Exception(sprintf('%s::%s at line %d - '.$message, $trace[1]['class'], $trace[1]['function'], $trace[0]['line']), $code);
    }
}
