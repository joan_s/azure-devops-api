<?php

namespace AzureDevOps;

use AzureDevOps\HttpClient\CurlClient;
use AzureDevOps\HttpClient\HttpClientInterface;

/**
 * Simple Azure DevOps Client.
 *
 * @property Api\Core            $core
 * @property Api\Work            $work
 * @property Api\Work\Iterations $workItemTracking
 *
 * @see https://gitlab.com/joan_s/php-azure-devops-api
 *
 * @author Joan Souliard <joan dot souliard at pm dot me>
 */
class Client
{
    use CreateExceptionTrait;

    private const HTTPS_PORT = 443;
    private const HTTP_GET = 'GET';
    private const HTTP_POST = 'POST';
    private const HTTP_PUT = 'PUT';
    private const HTTP_DELETE = 'DELETE';
    private const HTTP_PATCH = 'PATCH';

    public const APPLICATION_JSON = 'application/json';
    public const APPLICATION_JSON_PATCH = 'application/json-patch+json';
    public const APPLICATION_OCTET_STREAM = 'application/octet-stream';
    public const APPLICATION_FORM = 'application/x-www-form-urlencoded';

    /** @var HttpClientInterface */
    private $client;

    /** @var string */
    private $accessToken;

    /** @var bool */
    private $checkSSLCertificate;

    /** @var bool */
    private $checkSSLHost;

    /** @var int */
    private $sslVersion = 0;

    /** @var array Instantiated services. */
    private $services = [];

    /** @var array Available services classes. */
    private $classes = [
        'core' => 'Core',
        'workItemTracking' => 'WorkItemTracking',
        'work' => 'Work',
    ];

    /** @var string */
    private $url;

    /**
     * HTTP Status Codes.
     *
     * @var array
     */
    private static $httpStatus = [
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Time-out',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested range unsatisfiable',
        417 => 'Expectation failed',
        418 => 'I’m a teapot',
        421 => 'Bad mapping / Misdirected Request',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway ou Proxy Error',
        503 => 'Service Unavailable',
        504 => 'Gateway Time-out',
        505 => 'HTTP Version not supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient storage',
        508 => 'Loop detected',
        509 => 'Bandwidth Limit Exceeded',
        510 => 'Not extended',
        511 => 'Network authentication required',
    ];

    /**
     * Client's constructor.
     *
     * @param string $accessToken A valid Azure OAuth access token.
     * @param string $url Azure DevOps instance url must contains the organization (i.e: https://dev.azure.com/organization).
     * @param HttpClientInterface $httpClient Used for test purposes.
     */
    public function __construct(string $accessToken, string $url, ?HttpClientInterface $httpClient = null)
    {
        $this->accessToken = $accessToken;
        $this->url = $url;
        $this->client = empty($httpClient) ? new CurlClient() : $httpClient;
    }

    /**
     * PHP getter magic method.
     *
     * @param string $name
     *
     * @return Api\AbstractApi
     */
    public function __get(string $name)
    {
        return $this->service($name);
    }

    /**
     * Instantiate the service with the given name.
     *
     * @param string $name
     *
     * @throws \Exception
     *
     * @return Api\AbstractApi
     */
    public function service(string $name): Api\AbstractService
    {
        if (!isset($this->classes[$name])) {
            throw $this->createException(sprintf('Invalid Service name: %s.', $name));
        }

        if (isset($this->services[$name])) {
            return $this->services[$name];
        }

        $class = sprintf("AzureDevOps\Api\\%s", $this->classes[$name]);
        $this->services[$name] = new $class($this);

        return $this->services[$name];
    }

    /**
     * Turns on/off the ssl certificate validation.
     *
     * @param bool $check
     *
     * @return Client
     */
    public function setCheckSSLCertifcate(bool $check = false): self
    {
        $this->checkSSLCertificate = $check;

        return $this;
    }

    /**
     * Get ssl certificate validation flag.
     *
     * @return bool
     */
    public function getCheckSSLCertifcate(): bool
    {
        return $this->checkSSLCertificate;
    }

    /**
     * Get ssl host validation flag.
     *
     * @return bool
     */
    public function getCheckSSLHost(): bool
    {
        return $this->checkSSLHost;
    }

    /**
     * Turn on/off the ssl host validation.
     *
     * @return Client
     */
    public function setCheckSSLHost(bool $checkSSLHost = false): self
    {
        $this->checkSSLHost = $checkSSLHost;

        return $this;
    }

    /**
     * Get the value of sslVersion.
     *
     * @return int
     */
    public function getSslVersion(): int
    {
        return $this->sslVersion;
    }

    /**
     * Set the value of sslVersion.
     *
     * @return Client
     */
    public function setSslVersion(int $sslVersion = 0): self
    {
        $this->sslVersion = $sslVersion;

        return $this;
    }

    /**
     * Get Client's url.
     *
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * Execute a GET request.
     *
     * @param string $endpoint
     *
     * @return array
     */
    public function get(string $endpoint): array
    {
        return $this->runRequest($endpoint);
    }

    /**
     * Execute a POST request.
     *
     * @param string      $endpoint
     * @param array      $data
     * @param string|null $contentType
     *
     * @return array
     */
    public function post(string $endpoint, array $data = [], ?string $contentType = null): array
    {
        return $this->runRequest($endpoint, self::HTTP_POST, $data, $contentType);
    }

    /**
     * Execute a PATCH request.
     *
     * @param string      $endpoint
     * @param array       $data
     * @param string|null $contentType
     *
     * @return array
     */
    public function patch(string $endpoint, array $data = [], ?string $contentType = null): array
    {
        return $this->runRequest($endpoint, self::HTTP_PATCH, $data, $contentType);
    }

    /**
     * Execute cURL request.
     * TODO -- Refactor error handling.
     *
     * @param string      $endpoint
     * @param string      $method
     * @param array      $data
     * @param string|null $contentType
     *
     * @throws \Exception
     *
     * @return array
     */
    protected function runRequest(string $endpoint, string $method = self::HTTP_GET, array $data = [], ?string $contentType = null): array
    {
        $this->buildRequest($endpoint, $method, $data, $contentType);

        $response = $this->client->execute();

        // Handle curl errors.
        if ($this->client->errNo()) {
            $e = $this->createException($this->client->error(), $this->client->errNo());
            $this->client->close();
            throw $e;
        }

        // Handle HTTP status codes.
        $code = $this->client->getInfo(CURLINFO_HTTP_CODE);
        if ($code >= 400) {
            $contentType = $this->client->getInfo(CURLINFO_CONTENT_TYPE);
            if (false !== strpos($contentType, self::APPLICATION_JSON)) {
                $response = $this->decodeJson($response);

                if (isset($response['message'])) {
                    $message = $response['message'];
                }
            }

            $this->client->close();
            throw $this->createException(isset($message) ? $message : self::$httpStatus[$code], $code);
        }

        $this->client->close();

        return $this->decodeJson($response);
    }

    /**
     * Init cURL requests.
     *
     * @param string      $endpoint
     * @param string      $method
     * @param array      $data
     * @param string|null $contentType
     */
    protected function buildRequest(string $endpoint, string $method = self::HTTP_GET, array $data = [], ?string $contentType = null)
    {
        $headers = [];
        $this->client->init();

        $this->client->setOption(CURLOPT_VERBOSE, 0);
        $this->client->setOption(CURLOPT_HEADER, 0);
        $this->client->setOption(CURLOPT_RETURNTRANSFER, 1);

        // Set url.
        $this->client->setOption(CURLOPT_URL, $this->buildURL($endpoint));

        // Always use HTTPS.
        $this->client->setOption(CURLOPT_PORT, self::HTTPS_PORT);
        $this->client->setOption(CURLOPT_SSL_VERIFYPEER, $this->checkSSLCertificate);
        $this->client->setOption(CURLOPT_SSL_VERIFYHOST, $this->checkSSLHost);
        $this->client->setOption(CURLOPT_SSLVERSION, $this->sslVersion);

        // Always authenticate as basic auth.
        $this->client->setOption(CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        switch ($method) {
            case self::HTTP_POST:
                $this->client->setOption(CURLOPT_POST, 1);

                if (isset($data)) {
                    $this->setContent($data, $contentType);
                    array_push($headers, sprintf('Content-Type: %s', $contentType));
                }

                break;

            case self::HTTP_PATCH:
                $this->client->setOption(CURLOPT_CUSTOMREQUEST, self::HTTP_PATCH);

                if (isset($data)) {
                    $this->setContent($data, $contentType);
                    array_push($headers, sprintf('Content-Type: %s', $contentType));
                }

                break;

            case self::HTTP_GET:
            default:
                break;
        }

        // Add Authentication header.
        array_push($headers, sprintf('Authorization: Bearer %s', $this->accessToken));

        // Set headers.
        $this->client->setOption(CURLOPT_HTTPHEADER, $headers);
    }

    /**
     * Prepare the url for the given endpoint.
     *
     * @param string $endpoint
     *
     * @return string the full Endpoint URL
     */
    protected function buildURL(string $endpoint): string
    {
        return sprintf('%s/%s', $this->url, $endpoint);
    }

    /**
     * Prepare request content depending on the given content type.
     * By default, encode data in Json.
     *
     * @param array       $data
     * @param string|null $contentType
     */
    protected function setContent(array $data, ?string $contentType = null)
    {
        switch ($contentType) {
            case self::APPLICATION_OCTET_STREAM:
                $this->client->setOption(CURLOPT_INFILE, $data['file']);
                $this->client->setOption(CURLOPT_INFILESIZE, $data['size']);
                $content = $data['data'];
                break;

            case self::APPLICATION_FORM:
                $content = $this->encodeForm($data);
                break;

            default:
                $content = $this->encodeJson($data);
                break;
        }

        $this->client->setOption(CURLOPT_POSTFIELDS, $content);
    }

    /**
     * Decodes given json.
     *
     * @param string $json
     *
     * @throws \Exception
     *
     * @return array
     */
    protected function decodeJson(string $json): array
    {
        if (empty($json)) {
            return [];
        }

        $result = json_decode($json, true);
        if (null !== $result) {
            return $result;
        }

        throw $this->createException(json_last_error_msg(), json_last_error());
    }

    /**
     * Encodes given array into a json string.
     *
     * @param array $data
     *
     * @throws \Exception
     *
     * @return string
     */
    protected function encodeJson(array $data): string
    {
        if (empty($data)) {
            return '';
        }

        $result = json_encode($data);
        if ($result) {
            return $result;
        }

        throw $this->createException(json_last_error_msg(), json_last_error());
    }

    /**
     * Encodes given array into a form-url string.
     *
     * @param array $data
     *
     * @return string
     */
    protected function encodeForm(array $data): string
    {
        $content = '';
        foreach ($data as $key => $value) {
            $content .= $key.'='.$value.'&';
        }

        return rtrim($content, '&');
    }
}
