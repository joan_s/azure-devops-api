<?php

namespace AzureDevOps\HttpClient;

/**
 * Simple wrapper around PHP cURL implementation.
 */
class CurlClient implements HttpClientInterface
{
    /** @var resource */
    private $handle = null;

    /** @var array */
    private $curlOptions = [];

    /**
     * {@inheritDoc}
     */
    public function close()
    {
        curl_close($this->handle);

        $this->curlOptions = [];
        $this->handle = null;
    }

    /**
     * {@inheritDoc}
     */
    public function errNo(): int
    {
        return curl_errno($this->handle);
    }

    /**
     * {@inheritDoc}
     */
    public function error(): string
    {
        return curl_error($this->handle);
    }

    /**
     * {@inheritDoc}
     */
    public function execute()
    {
        if (empty($this->handle)) {
            $this->handle = curl_init();
        }

        curl_setopt_array($this->handle, $this->curlOptions);

        return curl_exec($this->handle);
    }

    /**
     * {@inheritDoc}
     */
    public function init(?string $url = null)
    {
        $this->handle = curl_init($url);
    }

    /**
     * {@inheritDoc}
     */
    public function getInfo(int $option)
    {
        return curl_getinfo($this->handle, $option);
    }

    /**
     * {@inheritDoc}
     */
    public function getOptions(): array
    {
        return $this->curlOptions;
    }

    /**
     * {@inheritDoc}
     */
    public function setOption(int $option, $value)
    {
        $this->curlOptions[$option] = $value;
    }

    /**
     * {@inheritDoc}
     */
    public function setOptionArray(array $options)
    {
        $this->curlOptions += $options;
    }

    /**
     * {@inheritDoc}
     */
    public function unsetOption(int $option)
    {
        unset($this->curlOptions[$option]);
    }
}
