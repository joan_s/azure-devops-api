<?php

namespace AzureDevOps\HttpClient;

interface HttpClientInterface
{
    /**
     * Close the HTTP session.
     */
    public function close();

    /**
     * Returns the last error number.
     *
     * @return integer
     */
    public function errNo(): int;

    /**
     * Returns a string containing the last error for the current session.
     *
     * @return string
     */
    public function error(): string;

    /**
     * Performs an HTTP sessions.
     */
    public function execute();

    /**
     * Get information regarding a specific transfer.
     *
     * @param integer $option
     *
     * @return mixed
     */
    public function getInfo(int $option);

    /**
     * Returns an array containing all sets options for the HTTP transfer.
     *
     * @return array
     */
    public function getOptions(): array;

    /**
     * Initialize an HTTP session.
     *
     * @param string $url
     */
    public function init(?string $url = null);

    /**
     * Set an option for an HTTP transfer.
     *
     * @param integer $option
     * @param mixed $value
     */
    public function setOption(int $option, $value);

    /**
     * Set multiple options for an HTTP transfer.
     *
     * @param array $options
     */
    public function setOptionArray(array $options);

    /**
     * Removes an option for the HTTP transfer.
     *
     * @param integer $option
     */
    public function unsetOption(int $option);
}
