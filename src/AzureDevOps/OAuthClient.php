<?php

namespace AzureDevOps;

/**
 * OAuth Client for Azure DevOps.
 *
 * @method array getTokens
 * @method array refreshTokens
 *
 * @author Joan Souliard <joan dot souliard at pm dot me>
 */
class OAuthClient extends Client
{
    private const ACCESS_TOKEN_URL = 'https://app.vssps.visualstudio.com';
    private const ENDPOINT = '/oauth2/token';
    private const FORM_GRANT_TYPE_BEARER = 'urn:ietf:params:oauth:grant-type:jwt-bearer';
    private const FORM_GRANT_TYPE_REFRESH_TOKEN = 'refresh_token';

    /** @var string */
    private $clientSecret;

    /** @var string */
    private $redirectURI;

    /**
     * OAuthClient Constructor.
     *
     * @param string $clientSecret
     * @param string $redirectURI
     */
    public function __construct(string $clientSecret, string $redirectURI)
    {
        $this->clientSecret = $clientSecret;
        $this->redirectURI = $redirectURI;

        parent::__construct('', self::ACCESS_TOKEN_URL);
    }

    /**
     * Request an access token using the given authorization code, or refresh token.
     *
     * @param string $codeOrToken Authorization code or refresh token.
     *
     * @return array
     */
    public function getOAuthTokens(string $codeOrToken): array
    {
        try {
            $response = $this->getTokenWithRefreshToken($codeOrToken);
        } catch (\Exception $e) {
            $response = $this->getTokenWithAuthorizationCode($codeOrToken);
        }

        return $response;
    }

    /**
     * Request an access token with the given authorization code.
     *
     * @param string $code
     *
     * @return array
     */
    private function getTokenWithAuthorizationCode(string $code): array
    {
        return $this->getToken($this->getFormData($code, true));
    }

    /**
     * Request an access token with the given refresh token.
     *
     * @param string $refreshToken
     *
     * @return array
     */
    private function getTokenWithRefreshToken(string $refreshToken): array
    {
        return $this->getToken($this->getFormData($refreshToken));
    }

    /**
     * Run a post request to get an access token, with the given data.
     *
     * @param array $formData
     *
     * @return array
     */
    private function getToken(array $formData): array
    {
        return $this->post(self::ENDPOINT, $formData, self::APPLICATION_FORM);
    }

    /**
     * Returns urlencoded form data expected by Azure OAuth for access token requests.
     *
     * @param string $codeOrRefreshToken
     * @param bool   $token
     *
     * @return array
     */
    protected function getFormData(string $codeOrRefreshToken, bool $bearer = false): array
    {
        $grantType = $bearer ? self::FORM_GRANT_TYPE_BEARER : self::FORM_GRANT_TYPE_REFRESH_TOKEN;

        return [
            'client_assertion_type' => 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer',
            'client_assertion' => urlencode($this->clientSecret),
            'grant_type' => $grantType,
            'assertion' => urlencode($codeOrRefreshToken),
            'redirect_uri' => urlencode($this->redirectURI),
        ];
    }
}
