<?php

namespace AzureDevOps\Tests\Unit;

use AzureDevOps\Client;

class ClientTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @covers \AzureDevOps\Client
     *
     * @test
     */
    public function instantiateClient()
    {
        $client = new Client('access_token', 'http://test.local');

        $this->assertInstanceOf('AzureDevOps\Client', $client);
    }

    /**
     * @covers \AzureDevOps\Client
     * @covers \AzureDevOps\CreateExceptionTrait
     *
     * @test
     */
    public function shouldNotGetServiceInstance()
    {
        $this->expectException('\Exception');

        $client = new Client('access_token', 'http://test.local');
        $client->do_not_exist;
    }

    /**
     * @covers \AzureDevOps\Api\AbstractService
     * @covers \AzureDevOps\Client
     *
     * @test
     */
    public function shouldGetServiceInstance()
    {
        $client = new Client('access_token', 'http://test.local');

        $this->assertInstanceOf('AzureDevOps\Api\Core', $client->core);
        $this->assertInstanceOf('AzureDevOps\Api\Work', $client->work);
        $this->assertInstanceOf('AzureDevOps\Api\WorkItemTracking', $client->workItemTracking);
    }

    /**
     * @covers \AzureDevOps\Client
     *
     * @test
     */
    public function getUrlReturnsValueFromConstructor()
    {
        $client = new Client('access_token', 'http://test.local');

        $this->assertSame('http://test.local', $client->getUrl());
    }

    /**
     * @covers \AzureDevOps\Client
     *
     * @test
     */
    public function getAndSetCheckSSLCertificate()
    {
        $client = new Client('access_token', 'http://test.local');

        $this->assertInstanceOf('AzureDevOps\Client', $client->setCheckSSLCertifcate());
        $this->assertFalse($client->getCheckSSLCertifcate());
        $this->assertInstanceOf('AzureDevOps\Client', $client->setCheckSSLCertifcate(true));
        $this->assertTrue($client->getCheckSSLCertifcate());
        $this->assertInstanceOf('AzureDevOps\Client', $client->setCheckSSLCertifcate(false));
        $this->assertFalse($client->getCheckSSLCertifcate());
    }

    /**
     * @covers \AzureDevOps\Client
     *
     * @test
     */
    public function getAndSetCheckSSLHost()
    {
        $client = new Client('access_token', 'http://test.local');

        $this->assertInstanceOf('AzureDevOps\Client', $client->setCheckSSLHost());
        $this->assertFalse($client->getCheckSSLHost());
        $this->assertInstanceOf('AzureDevOps\Client', $client->setCheckSSLHost(true));
        $this->assertTrue($client->getCheckSSLHost());
        $this->assertInstanceOf('AzureDevOps\Client', $client->setCheckSSLHost(false));
        $this->assertFalse($client->getCheckSSLHost());
    }

    /**
     * @covers \AzureDevOps\Client
     *
     * @test
     */
    public function getAndSetSSLVersion()
    {
        $client = new Client('access_token', 'http://test.local');

        $this->assertInstanceOf('AzureDevOps\Client', $client->setSslVersion());
        $this->assertSame(0, $client->getSslVersion());
        $this->assertInstanceOf('AzureDevOps\Client', $client->setSslVersion(1));
        $this->assertSame(1, $client->getSslVersion());
    }

    /**
     * @covers \AzureDevOps\Client
     *
     * @test
     */
    public function getWithValidJson()
    {
        $httpClient = $this->getMockBuilder('AzureDevOps\HttpClient\HttpClientInterface')->getMock();

        $expectedResult = [
            'count' => 1,
            'value' => [
                [
                    'name' => 'Test Project',
                    'id' => 'azertyui',
                ],
            ],
        ];
        $input = json_encode($expectedResult);
        $httpClient->expects($this->any())->method('execute')->willReturn($input);

        $client = new Client('access_token', 'https://test.local', $httpClient);
        $result = $client->get('endpoint');

        $this->assertSame($expectedResult, $result);
    }

    /**
     * @covers \AzureDevOps\Client
     *
     * @test
     */
    public function getWithInvalidJson()
    {
        $this->expectException('\Exception');

        $httpClient = $this->getMockBuilder('AzureDevOps\HttpClient\HttpClientInterface')->getMock();
        $httpClient->expects($this->any())->method('execute')->willReturn('{ "bad", "Json": {"test": "syntax", "error"}}');

        $client = new Client('access_token', 'https://test.local', $httpClient);
        $client->get('endpoint');
    }

    /**
     * @covers \AzureDevOps\Client
     *
     * @test
     */
    public function getWithHttpErrorAndJsonResponse()
    {
        $this->expectExceptionCode(400);
        $this->expectExceptionMessage('expected message');

        $httpClient = $this->getMockBuilder('AzureDevOps\HttpClient\HttpClientInterface')->getMock();
        $httpClient->expects($this->any())->method('execute')->willReturn('{"message": "expected message"}');
        $httpClient->expects($this->any())->method('getInfo')->will($this->returnCallback(function () {
            switch (func_get_args()[0]) {
                case CURLINFO_HTTP_CODE:
                    return 400;

                case CURLINFO_CONTENT_TYPE:
                    return Client::APPLICATION_JSON;
            }
        }));

        $client = new Client('access_token', 'http://test.local', $httpClient);
        $client->get('endpoint');
    }

    /**
     * @covers \AzureDevOps\Client
     *
     * @test
     */
    public function getWithHttpErrorAndUnknownResponse()
    {
        $this->expectExceptionCode(400);
        $this->expectExceptionMessage('Bad Request');

        $httpClient = $this->getMockBuilder('AzureDevOps\HttpClient\HttpClientInterface')->getMock();
        $httpClient->expects($this->any())->method('getInfo')->will($this->returnCallback(function () {
            switch (func_get_args()[0]) {
                case CURLINFO_HTTP_CODE:
                    return 400;

                case CURLINFO_CONTENT_TYPE:
                    return 'unknow';
            }
        }));

        $client = new Client('access_token', 'http://test.local', $httpClient);
        $client->get('endpoint');
    }

    /**
     * @covers \AzureDevOps\Client
     *
     * @test
     */
    public function getWithCurlError()
    {
        $this->expectExceptionCode(7);
        $this->expectExceptionMessage('CURL_CONNECTION_ERROR');

        $httpClient = $this->getMockBuilder('AzureDevOps\HttpClient\HttpClientInterface')->getMock();
        $httpClient->expects($this->any())->method('errNo')->willReturn(7);
        $httpClient->expects($this->any())->method('error')->willReturn('CURL_CONNECTION_ERROR');

        $client = new Client('access_token', 'http://test.local', $httpClient);
        $client->get('endpoint');
    }

    /**
     * @covers \AzureDevOps\Client
     *
     * @test
     */
    public function postWithValidJsonData()
    {
        $this->post(['id' => 1234, 'content' => 'azertyui'], Client::APPLICATION_JSON_PATCH);
    }

    /**
     * @covers \AzureDevOps\Client
     *
     * @test
     */
    public function postWithEmptyJsonData()
    {
        $this->post([], Client::APPLICATION_JSON_PATCH);
    }

    /**
     * @covers \AzureDevOps\Client
     *
     * @test
     */
    public function postWithValidFormData()
    {
        $this->post(['id' => 1234, 'content' => 'azertyui'], Client::APPLICATION_FORM);
    }

    /**
     * @covers \AzureDevOps\Client
     *
     * @test
     */
    public function postWithEmptyFormData()
    {
        $this->post([], Client::APPLICATION_FORM);
    }

    /**
     * @covers \AzureDevOps\Client
     *
     * @test
     */
    public function postWithFileData()
    {
        $this->post(['file' => 'handle', 'size' => 182999, 'data' => 'file_content'], Client::APPLICATION_OCTET_STREAM);
    }

    /**
     * @covers \AzureDevOps\Client
     *
     * @test
     */
    public function patchWithValidJsonData()
    {
        $expectedResult = [
            'id' => 1234,
            'summary' => 'issue test',
        ];
        $output = json_encode($expectedResult);

        $httpClient = $this->getMockBuilder('AzureDevOps\HttpClient\HttpClientInterface')->getMock();
        $httpClient->expects($this->any())->method('execute')->willReturn($output);

        $client = new Client('access_token', 'https://test.local', $httpClient);
        $result = $client->patch('endpoint', ['id' => 1234, 'content' => 'azertyui'], Client::APPLICATION_JSON_PATCH);

        $this->assertSame($expectedResult, $result);
    }

    /**
     * Execute a client post request with given data.
     *
     * @param array $data
     * @param string $contentType
     */
    private function post(array $data, string $contentType)
    {
        $expectedResult = [
            'id' => 1234,
            'summary' => 'issue test',
        ];
        $output = json_encode($expectedResult);

        $httpClient = $this->getMockBuilder('AzureDevOps\HttpClient\HttpClientInterface')->getMock();
        $httpClient->expects($this->any())->method('execute')->willReturn($output);

        $client = new Client('access_token', 'https://test.local', $httpClient);
        $result = $client->post('endpoint', $data, $contentType);

        $this->assertSame($expectedResult, $result);
    }
}
