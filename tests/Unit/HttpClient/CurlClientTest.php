<?php

namespace AzureDevOps\Tests\Unit\HttpClient;

use AzureDevOps\HttpClient\CurlClient;

class CurlClientTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @covers \AzureDevOps\HttpClient\CurlClient
     *
     * @test
     */
    public function getAndSetOptions()
    {
        $client = new CurlClient();

        $this->assertSame([], $client->getOptions());
        $client->setOption(5, 'test');
        $this->assertSame([5 => 'test'], $client->getOptions());
        $client->unsetOption(5);
        $this->assertSame([], $client->getOptions());
        $client->setOptionArray([5 => 'test']);
        $this->assertSame([5 => 'test'], $client->getOptions());
    }
}
